/* Generated By:JavaCC: Do not edit this line. SystemVerilogParserConstants.java */
package com.kpankov.sveet2.svlexer;


/**
 * Token literal values and constants.
 * Generated by org.javacc.parser.OtherFilesGen#start()
 */
public interface SystemVerilogParserConstants {

  /** End of File. */
  int EOF = 0;
  /** RegularExpression Id. */
  int WHITESPACE = 1;
  /** RegularExpression Id. */
  int SINGLE_LINE_COMMENT = 4;
  /** RegularExpression Id. */
  int FORMAL_COMMENT = 5;
  /** RegularExpression Id. */
  int MULTI_LINE_COMMENT = 6;
  /** RegularExpression Id. */
  int ALIAS = 8;
  /** RegularExpression Id. */
  int ALWAYS = 9;
  /** RegularExpression Id. */
  int ALWAYS_COMB = 10;
  /** RegularExpression Id. */
  int ALWAYS_FF = 11;
  /** RegularExpression Id. */
  int ALWAYS_LATCH = 12;
  /** RegularExpression Id. */
  int AND = 13;
  /** RegularExpression Id. */
  int ASSERT = 14;
  /** RegularExpression Id. */
  int ASSIGN = 15;
  /** RegularExpression Id. */
  int ASSUME = 16;
  /** RegularExpression Id. */
  int AUTOMATIC = 17;
  /** RegularExpression Id. */
  int BEFORE = 18;
  /** RegularExpression Id. */
  int BEGIN = 19;
  /** RegularExpression Id. */
  int BIND = 20;
  /** RegularExpression Id. */
  int BINS = 21;
  /** RegularExpression Id. */
  int BINSOF = 22;
  /** RegularExpression Id. */
  int BIT = 23;
  /** RegularExpression Id. */
  int BREAK = 24;
  /** RegularExpression Id. */
  int BUF = 25;
  /** RegularExpression Id. */
  int BUFIF0 = 26;
  /** RegularExpression Id. */
  int BUFIF1 = 27;
  /** RegularExpression Id. */
  int BYTE = 28;
  /** RegularExpression Id. */
  int CASE = 29;
  /** RegularExpression Id. */
  int CASEX = 30;
  /** RegularExpression Id. */
  int CASEZ = 31;
  /** RegularExpression Id. */
  int CELL = 32;
  /** RegularExpression Id. */
  int CHANDLE = 33;
  /** RegularExpression Id. */
  int CLASS = 34;
  /** RegularExpression Id. */
  int CLOCKING = 35;
  /** RegularExpression Id. */
  int CMOS = 36;
  /** RegularExpression Id. */
  int CONFIG = 37;
  /** RegularExpression Id. */
  int CONST = 38;
  /** RegularExpression Id. */
  int CONSTRAINT = 39;
  /** RegularExpression Id. */
  int CONTEXT = 40;
  /** RegularExpression Id. */
  int CONTINUE = 41;
  /** RegularExpression Id. */
  int COVER = 42;
  /** RegularExpression Id. */
  int COVERGROUP = 43;
  /** RegularExpression Id. */
  int COVERPOINT = 44;
  /** RegularExpression Id. */
  int CROSS = 45;
  /** RegularExpression Id. */
  int DEASSIGN = 46;
  /** RegularExpression Id. */
  int DEFAULT_KW = 47;
  /** RegularExpression Id. */
  int DEFPARAM = 48;
  /** RegularExpression Id. */
  int DESIGN = 49;
  /** RegularExpression Id. */
  int DISABLE = 50;
  /** RegularExpression Id. */
  int DIST = 51;
  /** RegularExpression Id. */
  int DO = 52;
  /** RegularExpression Id. */
  int EDGE = 53;
  /** RegularExpression Id. */
  int ELSE = 54;
  /** RegularExpression Id. */
  int END = 55;
  /** RegularExpression Id. */
  int ENDCASE = 56;
  /** RegularExpression Id. */
  int ENDCLASS = 57;
  /** RegularExpression Id. */
  int ENDCLOCKING = 58;
  /** RegularExpression Id. */
  int ENDCONFIG = 59;
  /** RegularExpression Id. */
  int ENDFUNCTION = 60;
  /** RegularExpression Id. */
  int ENDGENERATE = 61;
  /** RegularExpression Id. */
  int ENDGROUP = 62;
  /** RegularExpression Id. */
  int ENDINTERFACE = 63;
  /** RegularExpression Id. */
  int ENDMODULE = 64;
  /** RegularExpression Id. */
  int ENDPACKAGE = 65;
  /** RegularExpression Id. */
  int ENDPRIMITIVE = 66;
  /** RegularExpression Id. */
  int ENDPROGRAM = 67;
  /** RegularExpression Id. */
  int ENDPROPERTY = 68;
  /** RegularExpression Id. */
  int ENDSEQUENCE = 69;
  /** RegularExpression Id. */
  int ENDSPECIFY = 70;
  /** RegularExpression Id. */
  int ENDTABLE = 71;
  /** RegularExpression Id. */
  int ENDTASK = 72;
  /** RegularExpression Id. */
  int ENUM = 73;
  /** RegularExpression Id. */
  int EVENT = 74;
  /** RegularExpression Id. */
  int EXPECT = 75;
  /** RegularExpression Id. */
  int EXPORT = 76;
  /** RegularExpression Id. */
  int EXTENDS = 77;
  /** RegularExpression Id. */
  int EXTERN = 78;
  /** RegularExpression Id. */
  int FINAL = 79;
  /** RegularExpression Id. */
  int FIRST_MATCH = 80;
  /** RegularExpression Id. */
  int FOR = 81;
  /** RegularExpression Id. */
  int FORCE = 82;
  /** RegularExpression Id. */
  int FOREACH = 83;
  /** RegularExpression Id. */
  int FOREVER = 84;
  /** RegularExpression Id. */
  int FORK = 85;
  /** RegularExpression Id. */
  int FORKJOIN = 86;
  /** RegularExpression Id. */
  int FUNCTION = 87;
  /** RegularExpression Id. */
  int GENERATE = 88;
  /** RegularExpression Id. */
  int GENVAR = 89;
  /** RegularExpression Id. */
  int HIGHZ0 = 90;
  /** RegularExpression Id. */
  int HIGHZ1 = 91;
  /** RegularExpression Id. */
  int IF = 92;
  /** RegularExpression Id. */
  int IFF = 93;
  /** RegularExpression Id. */
  int IFNONE = 94;
  /** RegularExpression Id. */
  int IGNORE_BINS = 95;
  /** RegularExpression Id. */
  int ILLEGAL_BINS = 96;
  /** RegularExpression Id. */
  int IMPORT = 97;
  /** RegularExpression Id. */
  int INCDIR = 98;
  /** RegularExpression Id. */
  int INITIAL = 99;
  /** RegularExpression Id. */
  int INOUT = 100;
  /** RegularExpression Id. */
  int INPUT = 101;
  /** RegularExpression Id. */
  int INSIDE = 102;
  /** RegularExpression Id. */
  int INSTANCE = 103;
  /** RegularExpression Id. */
  int INT = 104;
  /** RegularExpression Id. */
  int INTEGER = 105;
  /** RegularExpression Id. */
  int INTERFACE = 106;
  /** RegularExpression Id. */
  int INTERSECT = 107;
  /** RegularExpression Id. */
  int JOIN = 108;
  /** RegularExpression Id. */
  int JOIN_ANY = 109;
  /** RegularExpression Id. */
  int JOIN_NONE = 110;
  /** RegularExpression Id. */
  int LARGE = 111;
  /** RegularExpression Id. */
  int LIBLIST = 112;
  /** RegularExpression Id. */
  int LIBRARY = 113;
  /** RegularExpression Id. */
  int LOCAL = 114;
  /** RegularExpression Id. */
  int LOCALPARAM = 115;
  /** RegularExpression Id. */
  int LOGIC = 116;
  /** RegularExpression Id. */
  int LONGINT = 117;
  /** RegularExpression Id. */
  int MACROMODULE = 118;
  /** RegularExpression Id. */
  int MATCHES = 119;
  /** RegularExpression Id. */
  int MEDIUM = 120;
  /** RegularExpression Id. */
  int MODPORT = 121;
  /** RegularExpression Id. */
  int MODULE = 122;
  /** RegularExpression Id. */
  int NAND = 123;
  /** RegularExpression Id. */
  int NEGEDGE = 124;
  /** RegularExpression Id. */
  int NEW = 125;
  /** RegularExpression Id. */
  int NMOS = 126;
  /** RegularExpression Id. */
  int NOR = 127;
  /** RegularExpression Id. */
  int NOSHOWCANCELLED = 128;
  /** RegularExpression Id. */
  int NOT = 129;
  /** RegularExpression Id. */
  int NOTIF0 = 130;
  /** RegularExpression Id. */
  int NOTIF1 = 131;
  /** RegularExpression Id. */
  int NULL = 132;
  /** RegularExpression Id. */
  int OR = 133;
  /** RegularExpression Id. */
  int OUTPUT = 134;
  /** RegularExpression Id. */
  int PACKAGE = 135;
  /** RegularExpression Id. */
  int PACKED = 136;
  /** RegularExpression Id. */
  int PARAMETER = 137;
  /** RegularExpression Id. */
  int PMOS = 138;
  /** RegularExpression Id. */
  int POSEDGE = 139;
  /** RegularExpression Id. */
  int PRIMITIVE = 140;
  /** RegularExpression Id. */
  int PRIORITY = 141;
  /** RegularExpression Id. */
  int PROGRAM = 142;
  /** RegularExpression Id. */
  int PROPERTY = 143;
  /** RegularExpression Id. */
  int PROTECTED = 144;
  /** RegularExpression Id. */
  int PULL0 = 145;
  /** RegularExpression Id. */
  int PULL1 = 146;
  /** RegularExpression Id. */
  int PULLDOWN = 147;
  /** RegularExpression Id. */
  int PULLUP = 148;
  /** RegularExpression Id. */
  int PULSESTYLE_ONDETECT = 149;
  /** RegularExpression Id. */
  int PULSESTYLE_ONEVENT = 150;
  /** RegularExpression Id. */
  int PURE = 151;
  /** RegularExpression Id. */
  int RAND = 152;
  /** RegularExpression Id. */
  int RANDC = 153;
  /** RegularExpression Id. */
  int RANDCASE = 154;
  /** RegularExpression Id. */
  int RANDSEQUENCE = 155;
  /** RegularExpression Id. */
  int RCMOS = 156;
  /** RegularExpression Id. */
  int REAL = 157;
  /** RegularExpression Id. */
  int REALTIME = 158;
  /** RegularExpression Id. */
  int REF = 159;
  /** RegularExpression Id. */
  int REG = 160;
  /** RegularExpression Id. */
  int RELEASE = 161;
  /** RegularExpression Id. */
  int REPEAT = 162;
  /** RegularExpression Id. */
  int RETURN = 163;
  /** RegularExpression Id. */
  int RNMOS = 164;
  /** RegularExpression Id. */
  int RPMOS = 165;
  /** RegularExpression Id. */
  int RTRAN = 166;
  /** RegularExpression Id. */
  int RTRANIF0 = 167;
  /** RegularExpression Id. */
  int RTRANIF1 = 168;
  /** RegularExpression Id. */
  int SCALARED = 169;
  /** RegularExpression Id. */
  int SEQUENCE = 170;
  /** RegularExpression Id. */
  int SHORTINT = 171;
  /** RegularExpression Id. */
  int SHORTREAL = 172;
  /** RegularExpression Id. */
  int SHOWCANCELLED = 173;
  /** RegularExpression Id. */
  int SIGNED = 174;
  /** RegularExpression Id. */
  int SMALL = 175;
  /** RegularExpression Id. */
  int SOLVE = 176;
  /** RegularExpression Id. */
  int SPECIFY = 177;
  /** RegularExpression Id. */
  int SPECPARAM = 178;
  /** RegularExpression Id. */
  int STATIC = 179;
  /** RegularExpression Id. */
  int STRING = 180;
  /** RegularExpression Id. */
  int STRONG0 = 181;
  /** RegularExpression Id. */
  int STRONG1 = 182;
  /** RegularExpression Id. */
  int STRUCT = 183;
  /** RegularExpression Id. */
  int SUPER = 184;
  /** RegularExpression Id. */
  int SUPPLY0 = 185;
  /** RegularExpression Id. */
  int SUPPLY1 = 186;
  /** RegularExpression Id. */
  int TABLE = 187;
  /** RegularExpression Id. */
  int TAGGED = 188;
  /** RegularExpression Id. */
  int TASK = 189;
  /** RegularExpression Id. */
  int THIS = 190;
  /** RegularExpression Id. */
  int THROUGHOUT = 191;
  /** RegularExpression Id. */
  int TIME = 192;
  /** RegularExpression Id. */
  int TIMEPRECISION = 193;
  /** RegularExpression Id. */
  int TIMEUNIT = 194;
  /** RegularExpression Id. */
  int TRAN = 195;
  /** RegularExpression Id. */
  int TRANIF0 = 196;
  /** RegularExpression Id. */
  int TRANIF1 = 197;
  /** RegularExpression Id. */
  int TRI = 198;
  /** RegularExpression Id. */
  int TRI0 = 199;
  /** RegularExpression Id. */
  int TRI1 = 200;
  /** RegularExpression Id. */
  int TRIAND = 201;
  /** RegularExpression Id. */
  int TRIOR = 202;
  /** RegularExpression Id. */
  int TRIREG = 203;
  /** RegularExpression Id. */
  int TYPE = 204;
  /** RegularExpression Id. */
  int TYPEDEF = 205;
  /** RegularExpression Id. */
  int UNION = 206;
  /** RegularExpression Id. */
  int UNIQUE = 207;
  /** RegularExpression Id. */
  int UNSIGNED = 208;
  /** RegularExpression Id. */
  int USE = 209;
  /** RegularExpression Id. */
  int UWIRE = 210;
  /** RegularExpression Id. */
  int VAR = 211;
  /** RegularExpression Id. */
  int VECTORED = 212;
  /** RegularExpression Id. */
  int VIRTUAL = 213;
  /** RegularExpression Id. */
  int VOID = 214;
  /** RegularExpression Id. */
  int WAIT = 215;
  /** RegularExpression Id. */
  int WAIT_ORDER = 216;
  /** RegularExpression Id. */
  int WAND = 217;
  /** RegularExpression Id. */
  int WEAK0 = 218;
  /** RegularExpression Id. */
  int WEAK1 = 219;
  /** RegularExpression Id. */
  int WHILE = 220;
  /** RegularExpression Id. */
  int WILDCARD = 221;
  /** RegularExpression Id. */
  int WIRE = 222;
  /** RegularExpression Id. */
  int WITH = 223;
  /** RegularExpression Id. */
  int WITHIN = 224;
  /** RegularExpression Id. */
  int WOR = 225;
  /** RegularExpression Id. */
  int XNOR = 226;
  /** RegularExpression Id. */
  int XOR = 227;
  /** RegularExpression Id. */
  int INTEGER_LITERAL = 228;
  /** RegularExpression Id. */
  int SIMPLE_NUM_LITERAL = 229;
  /** RegularExpression Id. */
  int DECIMAL_LITERAL = 230;
  /** RegularExpression Id. */
  int HEX_LITERAL = 231;
  /** RegularExpression Id. */
  int OCTAL_LITERAL = 232;
  /** RegularExpression Id. */
  int BINARY_LITERAL = 233;
  /** RegularExpression Id. */
  int FLOATING_POINT_LITERAL = 234;
  /** RegularExpression Id. */
  int DECIMAL_FLOATING_POINT_LITERAL = 235;
  /** RegularExpression Id. */
  int DECIMAL_EXPONENT = 236;
  /** RegularExpression Id. */
  int HEXADECIMAL_FLOATING_POINT_LITERAL = 237;
  /** RegularExpression Id. */
  int HEXADECIMAL_EXPONENT = 238;
  /** RegularExpression Id. */
  int CHARACTER_LITERAL = 239;
  /** RegularExpression Id. */
  int STRING_LITERAL = 240;
  /** RegularExpression Id. */
  int DELAY = 241;
  /** RegularExpression Id. */
  int IDENTIFIER = 242;
  /** RegularExpression Id. */
  int LETTER = 243;
  /** RegularExpression Id. */
  int PART_LETTER = 244;
  /** RegularExpression Id. */
  int LPAREN = 245;
  /** RegularExpression Id. */
  int RPAREN = 246;
  /** RegularExpression Id. */
  int LBRACE = 247;
  /** RegularExpression Id. */
  int RBRACE = 248;
  /** RegularExpression Id. */
  int LBRACKET = 249;
  /** RegularExpression Id. */
  int RBRACKET = 250;
  /** RegularExpression Id. */
  int SEMICOLON = 251;
  /** RegularExpression Id. */
  int COMMA = 252;
  /** RegularExpression Id. */
  int DOT = 253;
  /** RegularExpression Id. */
  int AT = 254;
  /** RegularExpression Id. */
  int OPERATOR_BLOCKINGASSIGN = 255;
  /** RegularExpression Id. */
  int OPERATOR_NONBLOCKINGASSIGN = 256;
  /** RegularExpression Id. */
  int OPERATOR_LT = 257;
  /** RegularExpression Id. */
  int OPERATOR_MT = 258;
  /** RegularExpression Id. */
  int OPERATOR_BANG = 259;
  /** RegularExpression Id. */
  int OPERATOR_TILDE = 260;
  /** RegularExpression Id. */
  int OPERATOR_HOOK = 261;
  /** RegularExpression Id. */
  int OPERATOR_COLON = 262;
  /** RegularExpression Id. */
  int OPERATOR_EQ = 263;
  /** RegularExpression Id. */
  int OPERATOR_LE = 264;
  /** RegularExpression Id. */
  int OPERATOR_GE = 265;
  /** RegularExpression Id. */
  int OPERATOR_NE = 266;
  /** RegularExpression Id. */
  int OPERATOR_SC_OR = 267;
  /** RegularExpression Id. */
  int OPERATOR_SC_AND = 268;
  /** RegularExpression Id. */
  int OPERATOR_INCR = 269;
  /** RegularExpression Id. */
  int OPERATOR_DECR = 270;
  /** RegularExpression Id. */
  int OPERATOR_PLUS = 271;
  /** RegularExpression Id. */
  int OPERATOR_MINUS = 272;
  /** RegularExpression Id. */
  int OPERATOR_STAR = 273;
  /** RegularExpression Id. */
  int OPERATOR_SLASH = 274;
  /** RegularExpression Id. */
  int OPERATOR_BIT_AND = 275;
  /** RegularExpression Id. */
  int OPERATOR_BIT_OR = 276;
  /** RegularExpression Id. */
  int OPERATOR_BIT_XOR = 277;
  /** RegularExpression Id. */
  int OPERATOR_REM = 278;
  /** RegularExpression Id. */
  int OPERATOR_LSHIFT = 279;
  /** RegularExpression Id. */
  int OPERATOR_PLUSASSIGN = 280;
  /** RegularExpression Id. */
  int OPERATOR_MINUSASSIGN = 281;
  /** RegularExpression Id. */
  int OPERATOR_STARASSIGN = 282;
  /** RegularExpression Id. */
  int OPERATOR_SLASHASSIGN = 283;
  /** RegularExpression Id. */
  int OPERATOR_ANDASSIGN = 284;
  /** RegularExpression Id. */
  int OPERATOR_ORASSIGN = 285;
  /** RegularExpression Id. */
  int OPERATOR_XORASSIGN = 286;
  /** RegularExpression Id. */
  int OPERATOR_REMASSIGN = 287;
  /** RegularExpression Id. */
  int OPERATOR_LSHIFTASSIGN = 288;
  /** RegularExpression Id. */
  int OPERATOR_RSIGNEDSHIFTASSIGN = 289;
  /** RegularExpression Id. */
  int OPERATOR_RUNSIGNEDSHIFTASSIGN = 290;
  /** RegularExpression Id. */
  int OPERATOR_WTF0 = 291;
  /** RegularExpression Id. */
  int OPERATOR_WTF1 = 292;
  /** RegularExpression Id. */
  int OPERATOR_WTF2 = 293;
  /** RegularExpression Id. */
  int PREPROCESSOR = 294;
  /** RegularExpression Id. */
  int PREPROCESSOR_INSERTION = 295;
  /** RegularExpression Id. */
  int PREPROCESSOR_LETTER = 296;
  /** RegularExpression Id. */
  int SYS = 297;
  /** RegularExpression Id. */
  int TYPECAST = 298;
  /** RegularExpression Id. */
  int TYPECAST_TYPE = 299;
  /** RegularExpression Id. */
  int UNDEFINED = 300;
  /** RegularExpression Id. */
  int FILL = 301;
  /** RegularExpression Id. */
  int VECTOR = 302;

  /** Lexical state. */
  int DEFAULT = 0;
  /** Lexical state. */
  int IN_FORMAL_COMMENT = 1;
  /** Lexical state. */
  int IN_MULTI_LINE_COMMENT = 2;

  /** Literal token values. */
  String[] tokenImage = {
    "<EOF>",
    "<WHITESPACE>",
    "<token of kind 2>",
    "\"/*\"",
    "<SINGLE_LINE_COMMENT>",
    "\"*/\"",
    "\"*/\"",
    "<token of kind 7>",
    "\"alias\"",
    "\"always\"",
    "\"always_comb\"",
    "\"always_ff\"",
    "\"always_latch\"",
    "\"and\"",
    "\"assert\"",
    "\"assign\"",
    "\"assume\"",
    "\"automatic\"",
    "\"before\"",
    "\"begin\"",
    "\"bind\"",
    "\"bins\"",
    "\"binsof\"",
    "\"bit\"",
    "\"break\"",
    "\"buf\"",
    "\"bufif0\"",
    "\"bufif1\"",
    "\"byte\"",
    "\"case\"",
    "\"casex\"",
    "\"casez\"",
    "\"cell\"",
    "\"chandle\"",
    "\"class\"",
    "\"clocking\"",
    "\"cmos\"",
    "\"config\"",
    "\"const\"",
    "\"constraint\"",
    "\"context\"",
    "\"continue\"",
    "\"cover\"",
    "\"covergroup\"",
    "\"coverpoint\"",
    "\"cross\"",
    "\"deassign\"",
    "\"default\"",
    "\"defparam\"",
    "\"design\"",
    "\"disable\"",
    "\"dist\"",
    "\"do\"",
    "\"edge\"",
    "\"else\"",
    "\"end\"",
    "\"endcase\"",
    "\"endclass\"",
    "\"endclocking\"",
    "\"endconfig\"",
    "\"endfunction\"",
    "\"endgenerate\"",
    "\"endgroup\"",
    "\"endinterface\"",
    "\"endmodule\"",
    "\"endpackage\"",
    "\"endprimitive\"",
    "\"endprogram\"",
    "\"endproperty\"",
    "\"endsequence\"",
    "\"endspecify\"",
    "\"endtable\"",
    "\"endtask\"",
    "\"enum\"",
    "\"event\"",
    "\"expect\"",
    "\"export\"",
    "\"extends\"",
    "\"extern\"",
    "\"final\"",
    "\"first_match\"",
    "\"for\"",
    "\"force\"",
    "\"foreach\"",
    "\"forever\"",
    "\"fork\"",
    "\"forkjoin\"",
    "\"function\"",
    "\"generate\"",
    "\"genvar\"",
    "\"highz0\"",
    "\"highz1\"",
    "\"if\"",
    "\"iff\"",
    "\"ifnone\"",
    "\"ignore_bins\"",
    "\"illegal_bins\"",
    "\"import\"",
    "\"incdir\"",
    "\"initial\"",
    "\"inout\"",
    "\"input\"",
    "\"inside\"",
    "\"instance\"",
    "\"int\"",
    "\"integer\"",
    "\"interface\"",
    "\"intersect\"",
    "\"join\"",
    "\"join_any\"",
    "\"join_none\"",
    "\"large\"",
    "\"liblist\"",
    "\"library\"",
    "\"local\"",
    "\"localparam\"",
    "\"logic\"",
    "\"longint\"",
    "\"macromodule\"",
    "\"matches\"",
    "\"medium\"",
    "\"modport\"",
    "\"module\"",
    "\"nand\"",
    "\"negedge\"",
    "\"new\"",
    "\"nmos\"",
    "\"nor\"",
    "\"noshowcancelled\"",
    "\"not\"",
    "\"notif0\"",
    "\"notif1\"",
    "\"null\"",
    "\"or\"",
    "\"output\"",
    "\"package\"",
    "\"packed\"",
    "\"parameter\"",
    "\"pmos\"",
    "\"posedge\"",
    "\"primitive\"",
    "\"priority\"",
    "\"program\"",
    "\"property\"",
    "\"protected\"",
    "\"pull0\"",
    "\"pull1\"",
    "\"pulldown\"",
    "\"pullup\"",
    "\"pulsestyle_ondetect\"",
    "\"pulsestyle_onevent\"",
    "\"pure\"",
    "\"rand\"",
    "\"randc\"",
    "\"randcase\"",
    "\"randsequence\"",
    "\"rcmos\"",
    "\"real\"",
    "\"realtime\"",
    "\"ref\"",
    "\"reg\"",
    "\"release\"",
    "\"repeat\"",
    "\"return\"",
    "\"rnmos\"",
    "\"rpmos\"",
    "\"rtran\"",
    "\"rtranif0\"",
    "\"rtranif1\"",
    "\"scalared\"",
    "\"sequence\"",
    "\"shortint\"",
    "\"shortreal\"",
    "\"showcancelled\"",
    "\"signed\"",
    "\"small\"",
    "\"solve\"",
    "\"specify\"",
    "\"specparam\"",
    "\"static\"",
    "\"string\"",
    "\"strong0\"",
    "\"strong1\"",
    "\"struct\"",
    "\"super\"",
    "\"supply0\"",
    "\"supply1\"",
    "\"table\"",
    "\"tagged\"",
    "\"task\"",
    "\"this\"",
    "\"throughout\"",
    "\"time\"",
    "\"timeprecision\"",
    "\"timeunit\"",
    "\"tran\"",
    "\"tranif0\"",
    "\"tranif1\"",
    "\"tri\"",
    "\"tri0\"",
    "\"tri1\"",
    "\"triand\"",
    "\"trior\"",
    "\"trireg\"",
    "\"type\"",
    "\"typedef\"",
    "\"union\"",
    "\"unique\"",
    "\"unsigned\"",
    "\"use\"",
    "\"uwire\"",
    "\"var\"",
    "\"vectored\"",
    "\"virtual\"",
    "\"void\"",
    "\"wait\"",
    "\"wait_order\"",
    "\"wand\"",
    "\"weak0\"",
    "\"weak1\"",
    "\"while\"",
    "\"wildcard\"",
    "\"wire\"",
    "\"with\"",
    "\"within\"",
    "\"wor\"",
    "\"xnor\"",
    "\"xor\"",
    "<INTEGER_LITERAL>",
    "<SIMPLE_NUM_LITERAL>",
    "<DECIMAL_LITERAL>",
    "<HEX_LITERAL>",
    "<OCTAL_LITERAL>",
    "<BINARY_LITERAL>",
    "<FLOATING_POINT_LITERAL>",
    "<DECIMAL_FLOATING_POINT_LITERAL>",
    "<DECIMAL_EXPONENT>",
    "<HEXADECIMAL_FLOATING_POINT_LITERAL>",
    "<HEXADECIMAL_EXPONENT>",
    "<CHARACTER_LITERAL>",
    "<STRING_LITERAL>",
    "<DELAY>",
    "<IDENTIFIER>",
    "<LETTER>",
    "<PART_LETTER>",
    "\"(\"",
    "\")\"",
    "\"{\"",
    "\"}\"",
    "\"[\"",
    "\"]\"",
    "\";\"",
    "\",\"",
    "\".\"",
    "\"@\"",
    "\"=\"",
    "\":=\"",
    "\"<\"",
    "\">\"",
    "\"!\"",
    "\"~\"",
    "\"?\"",
    "\":\"",
    "\"==\"",
    "\"<=\"",
    "\">=\"",
    "\"!=\"",
    "\"||\"",
    "\"&&\"",
    "\"++\"",
    "\"--\"",
    "\"+\"",
    "\"-\"",
    "\"*\"",
    "\"/\"",
    "\"&\"",
    "\"|\"",
    "\"^\"",
    "\"%\"",
    "\"<<\"",
    "\"+=\"",
    "\"-=\"",
    "\"*=\"",
    "\"/=\"",
    "\"&=\"",
    "\"|=\"",
    "\"^=\"",
    "\"%=\"",
    "\"<<=\"",
    "\">>=\"",
    "\">>>=\"",
    "\"`\\\"\"",
    "\"`\\\\`\\\"\"",
    "\"``\"",
    "<PREPROCESSOR>",
    "<PREPROCESSOR_INSERTION>",
    "<PREPROCESSOR_LETTER>",
    "<SYS>",
    "<TYPECAST>",
    "<TYPECAST_TYPE>",
    "<UNDEFINED>",
    "<FILL>",
    "<VECTOR>",
  };

}
