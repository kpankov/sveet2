/**
 * @author Konstantin Pankov <kspzel@gmail.com>
 * @date 26.02.2019
 */
package com.kpankov.sveet2;

import org.netbeans.api.lexer.Language;
import org.netbeans.modules.csl.spi.DefaultLanguageConfig;
import org.netbeans.modules.csl.spi.LanguageRegistration;
import com.kpankov.sveet2.lexer.SVTokenId;

@LanguageRegistration(mimeType = "text/x-systemverilog")
public class SVLanguage extends DefaultLanguageConfig {

    @Override
    public Language getLexerLanguage() {
        return SVTokenId.getLanguage();
    }

    @Override
    public String getDisplayName() {
        return "SystemVerilog";
    }

}
