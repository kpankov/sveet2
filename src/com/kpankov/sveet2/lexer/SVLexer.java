/**
 * @author Konstantin Pankov <kspzel@gmail.com>
 * @date 26.02.2019
 */
package com.kpankov.sveet2.lexer;

import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;
import com.kpankov.sveet2.svlexer.JavaCharStream;
import com.kpankov.sveet2.svlexer.SystemVerilogParserTokenManager;
import com.kpankov.sveet2.svlexer.Token;

/**
 *
 */
public class SVLexer implements Lexer<SVTokenId> {

    private LexerRestartInfo<SVTokenId> info;
    private SystemVerilogParserTokenManager javaParserTokenManager;

    SVLexer(LexerRestartInfo<SVTokenId> info) {
        this.info = info;
        JavaCharStream stream = new JavaCharStream(info.input());
        javaParserTokenManager = new SystemVerilogParserTokenManager(stream);
    }

    @Override
    public org.netbeans.api.lexer.Token<SVTokenId> nextToken() {
        Token token = javaParserTokenManager.getNextToken();
        if (info.input().readLength() < 1) {
            return null;
        }
        return info.tokenFactory().createToken(SVLanguageHierarchy.getToken(token.kind));
    }

    @Override
    public Object state() {
        return null;
    }

    @Override
    public void release() {
    }

}
